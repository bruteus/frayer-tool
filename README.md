# FRAYER tool

First Rapid And Yet Effective Refactoring tool is an utility to
quickly mass-reproduce the same program with different coding style,
variable/function/class names and more smaller details.

It's particularly useful for CS students which are the leaders in
terms of programming knowledge in their class and want to easily and
effortlessly help the other colleagues with code-autism to graduate.
