# You want to contribute - great!

**FRAYER Tool** modular design makes it very simple to extend its
functionality. Everything you need to write a module is to understand
a few things. Let me now explain them to you.

## Base class - Refactorator
On the top of everything stands the `Refactorator` class. Every single
module must inherit from it directly. It defines how the input code
will be processed, which I also present in this list:

1. In the beginning of `__init__` method execution the entire code
   is extracted into the `_code` field
2. Next step is to initialize a few fields with empty dict/list:

	* `_original` → an one-dimensional list which holds unchanged
      elements of the code
	* `_altered` → an one-dimesional list which holds refactored elements
      of the code; should be same length as `_original`
	* `_style` → a string which holds the names of the determined
      style of the code

3. Then we need to initialize a three key variables:

	* `STYLES` → a one-dimensional list of all style names related to
      the module
	* `OPPOSITES` → a dict which contains pairs: `style:
      opposite_style`; if there are styles which you think are
      opposites - here is their place-to-be
	* `CONVERSIONS` → a dict which holds pairs: `'from-to':
      conversion_function`, where `'from'` is name of the style which
      we want to convert to `'to'` which is our destination style and
      `conversion_function` is static method defined inside of the
      module

4. After `__init__` method, we have the following methods:

	* `scan` → searches for elements which we want to refactor and
      puts them into the `_original` list
	* `determine_style` → determines coding style based on the
      elements in the `_original`

5. Then there's the `refactor` method which basically uses information
   in `STYLES`, `OPPOSITES` and `CONVERSIONS` fields in order to
   perform actual refactorization on elements stored in `_original`
   and put the results in `_altered`
6. In the end, there are some functions which are less important:
	* `commit_changes` → permanently changes the `_code` substituting
      all elements from `_original` with those from `_altered`
	* `save_to_file` → saves `_code` to file specified in parameter

Note that in your module you need to complete only three steps:

* Initialization of `STYLES`, `OPPOSITES` and `CONVERSIONS`
* Implementation of conversion method for each style pair
* Implementation of the `scan` and `determine_style` methods

This seems not to be a lot of work, isn't it? That's all about
`Refactorator` class. Let's move to the next section.

## Writing a custom module
In this paragraph I will show you how to write your first **FRAYER
Tool** module in a step-by-step guide. In this example I will focus on
switching between different styles of closing so-called
*self-closeable* or *void tags* in HTML. So without further adieu
let's start!

### Step 1: Determine the problem
You've probably seen self-closeable tags in your IT career. I will
present a few of them to you now: `<br>`, `<img>`, `<meta>`. The thing
is some people want their code to follow the strict rules of XHTML and
write them in this manner: `<tag_name />`. If it comes to the module
name, I'm gonna use `SCTagsRef` for this purpose.

### Step 2: Identify the coding styles
As you probably see, we've encountered two coding styles in here: some
people want to close void tags, some don't. I think the best names for
them would be: `html` and `xhtml`. Having this in mind we now can add
this line to our module's `__init__` method:

```python
self.STYLES = ['html', 'xhtml']
```

### Step 3: Distinguish opposite styles
In our case this step is simple: we have only two coding styles so we
can mark them as opposite to each other by writing the next lines of
code:

```python
self.OPPOSITES = {
	'html': 'xhtml',
	'xhtml': 'html'
}
```

Things can get complicated if there is more than two styles or they
don't actually differ that much. In such cases you need to think which
of those are the opposites and include them in `OPPOSITES` or don't
include any styles at all.

This step isn't that much important. It's only there to help the
`refactor` method choose prefered style. If you want to learn more
just look for `refactor.py` file in this repo. It holds the
`Refactorator` class with docstrings attached so you can see how the
code actually works. If you don't want to dive into details, just
assume that initializing `OPPOSITES` is another thing you must do in
order to write a complete **FRAYER Tool** module.

### Step 4: Create conversion functions
This step is probably the largest part of writing a module and it
grows in terms of time consumption when the number of different styles
is getting larger. The first thing we need to do is to initialize the
`CONVERSIONS` dictionary which maps conversions to conversion
functions.  Let's see how this would look in the code:

```python
self.CONVERSIONS = {
	'html-xhtml': _cnv_html_to_xhtml,
	'xhtml-html': _cnv_xhtml_to_html
}
```

What does this mean? Take the first pair: `'html-xhtml'` means that we
will specify a function which will perform a conversion from `html` to
`xhtml` style. As you probably noticed: `_cnv_html_to_xhtml` is that
function. Conversion functions should be named in this particular way
to keep the code nice and coherent.

Ok, we've pointed out some conversion functions and now we have to
write them down. I'm gonna use the `re` module for python to
accomplish this task but it isn't enforced. Non-regural-expression
approach is also valid, depends on the case. Here's the code for both
conversion functions:

```python
# from re import compile (top of the file)

_html_close_tag_regex = compile('\s*(?=>)')
@staticmethod
def _cnv_html_to_xhtml(element):
	return SCTagsRef._html_close_tag_regex.sub(' /', element)


_xhtml_close_tag_regex = compile('(\s*/)(?=>)')
@staticmethod
def _cnv_xhtml_to_html(element):
	return SCTagsRef._xhtml_close_tag_regex.sub('', element)
```

Each of conversion functions has to have only one parameter which is a
string we want to convert from one style to another.

### Step 5: Write the `scan` method
Now when we have all conversion stuff done, we can move over to
filling the `_original` element list (which holds code elements that
we want to operate on in our module). To do that, let me show to you
an example HTML code and what elements specifically we want to extract
from it:

```html
<html>
	<head>
		<title>Example</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<p>Example text...</p>
		<br /><br />
		<img src="example.jpg" alt="an example" />
		<h1>Examples are very important</h1>
	</body>
</html>
```

Let's recall now, what our module is responsible for? It was something
about handling self-closeable tags and XHTML and HTML way to write
them. Can you see any examples of this type of tags in the code above?
Let's list them now:

* `<meta charset="utf-8" />`
* `<br />`
* `<br />`
* `<img src="example.jpg" alt="an example" />`

And such a list is exactly what we're expecting from the `scan` method
to return. Let me show you how this would look like in the python
code (I've assumed that the `re` module is imported):

```python
SELF_CLOSEABLE_TAGS = [
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input',
        'keygen', 'link', 'meta', 'parabm', 'source', 'track', 'wbr'
]

def scan(self):
	for sc_tag in SCTagsRef.SELF_CLOSEABLE_TAGS:
		found_elements = findall('<' + sc_tag + '[^>]*>', self._code)
		self._original.extend(found_elements)
```

First, we need to define what tags specifically we want to search
for. `SELF_CLOSEABLE_TAGS` has all of them inside. Next, we need to
implement the very function, which contains a single `for` loop in
which we iterate over tag names and form a regular expressions with
them. Then we use those expressions to find what we want in the
`_code` and extend the `_original` list with results.

### Step 6: Implement the `determine_style` method
The last step is to create an actual method to look at the elements we
extracted from the code and determine which coding style they
match. In our case this is very simple. We simply want to tell if the
tag is closed or not. So let's move straight to the implementation:

```python
def determine_style(self):
	html_style_occ = 0  # how many occurences of each style
	xhtml_style_occ = 0
	
	close_tag_regex = re.compile('/(?=>)')
	for sc_tag in self._original:
		if close_tag_regex.search(sc_tag) is None:
			html_style_occ += 1
		else:
			xhtml_style_occ += 1
	
	if html_style_occ != 0 and xhtml_style_occ == 0:
		self._style = 'html'
	elif xhtml_style_occ != 0 and html_style_occ == 0:
		self._style = 'xhtml'
	else:
		self._style = 'incohesive'
```

Again, it's only a basic regexp stuff. You may noticed that there's
one style we haven't discussed earlier. `incohesive` style is when
there is lack of consequence in style usage. For example in the code
we can have both: `<br>` and `<br />`. All of the elements from
`_original` must be written in the same coding style in order to
actually have any style determined by this method.

## Summary
As you can see, writing a module in the **FRAYER Tool** isn't that
complicated. If you want to learn more details about the underneath
operations, simply review the `Refactorator` class source code and/or
other modules already written.

Thank you for reading and I hope you will help to make this tool more
useful!


*~ bruteus*
