from refactor import Refactorator
from re import compile, findall
from sys import argv

class UpperLowerTagsRef(Refactorator):
    def __init__(self, filepath):
        super().__init__(filepath)

        self.STYLES = ['upper', 'lower']
        self.OPPOSITES = {
            'upper': 'lower',
            'lower': 'upper'
        }
        self.CONVERSIONS = {
            'upper-lower': UpperLowerTagsRef._cnv_upper_to_lower,
            'lower-upper': UpperLowerTagsRef._cnv_lower_to_upper
        }


    # Matches 'title' in <title> or </title>
    _tag_name_regex = compile('(?<=<)[a-zA-Z1-6]+|(?<=<\/)[a-zA-Z1-6]+')

    @staticmethod
    def _cnv_upper_to_lower(element):
        tag_name = UpperLowerTagsRef._tag_name_regex.findall(element)[0]
        return element.replace(tag_name, tag_name.lower(), 1)


    @staticmethod
    def _cnv_lower_to_upper(element):
        tag_name = UpperLowerTagsRef._tag_name_regex.findall(element)[0]
        return element.replace(tag_name, tag_name.upper(), 1)


    def scan(self):
        found_elements = findall('(<[a-zA-Z1-6]+[^>]*>)|(<\/[a-zA-Z1-6]+>)', self._code)
        self._original = [
            group_item \
            for group in found_elements \
            for group_item in group \
            if group_item != ''
        ]


    def determine_style(self):
        upper_style_occ = 0
        lower_style_occ = 0

        for tag in self._original:
            if UpperLowerTagsRef._tag_name_regex.findall(tag)[0].isupper():
                upper_style_occ += 1
            else:
                lower_style_occ += 1

        if upper_style_occ != 0 and lower_style_occ == 0:
            self._style = 'upper'
        elif lower_style_occ != 0 and upper_style_occ == 0:
            self._style = 'lower'
        else:
            self._style = 'incohesive'


# END OF CLASS DECLARATION

def main():
    if len(argv) != 3:
        print('Usage: python3 %s <original filepath> <new filepath>' % argv[0])
        return

    original_filepath = argv[1]
    new_filepath = argv[2]

    upperlowertags_ref = UpperLowerTagsRef(original_filepath)
    upperlowertags_ref.scan()
    upperlowertags_ref.determine_style()
    upperlowertags_ref.refactor('opposite')
    upperlowertags_ref.commit_changes()
    upperlowertags_ref.save_to_file(new_filepath)

if __name__ == '__main__':
    main()
