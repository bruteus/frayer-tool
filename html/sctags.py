from re import findall, compile
from random import choice
from sys import argv
from refactor import Refactorator

class SCTagsRef(Refactorator):
    """Handles self-closeable tags styles in HTML code"""
    SELF_CLOSEABLE_TAGS = [
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input',
        'keygen', 'link', 'meta', 'parabm', 'source', 'track', 'wbr'
    ]

    def __init__(self, filepath):
        super().__init__(filepath)

        self.STYLES = ['html', 'xhtml']
        self.OPPOSITES = {
            'html': 'xhtml',
            'xhtml': 'html'
        }
        self.CONVERSIONS = {
            'html-xhtml': self._cnv_html_to_xhtml,
            'xhtml-html': self._cnv_xhtml_to_html
        }


    _html_end_tag_regex = compile('\s*(?=>)')
    @staticmethod
    def _cnv_html_to_xhtml(element):
        return SCTagsRef._html_end_tag_regex.sub(' /', element)


    _xhtml_end_tag_regex = compile('(\s*/)(?=>)')
    @staticmethod
    def _cnv_xhtml_to_html(element):
        return SCTagsRef._xhtml_end_tag_regex.sub('', element)


    def scan(self):
        """Searches the `code` for the existence of self-closeable tags

        Saves all ocurrences in one-dimensional list (`original`).
        """
        for sc_tag in SCTagsRef.SELF_CLOSEABLE_TAGS:
            found_elements = findall('<' + sc_tag + '[^>]*>', self._code)
            self._original.extend(found_elements)


    def determine_style(self):
        """Determines `style` based on `original` tags

        For example:
        - <br><br> is 'html' style
        - <br/><br/> is 'xhtml' style
        - <br><br/> is 'incohesive' style
        """
        html_style_occ = 0  # how many occurences of each style
        xhtml_style_occ = 0

        close_tag_regex = compile('/(?=>)')
        for tag in self._original:
            if close_tag_regex.search(tag) is None:
                html_style_occ += 1
            else:
                xhtml_style_occ += 1

        if html_style_occ != 0 and xhtml_style_occ == 0:
            self._style = 'html'
        elif xhtml_style_occ != 0 and html_style_occ == 0:
            self._style = 'xhtml'
        else:
            self._style = 'incohesive'


### END OF CLASS DECLARATION

def main():
    if len(argv) != 3:
        print('Usage: python3 %s <original filepath> <new filepath>' % argv[0])
        return

    original_filepath = argv[1]
    new_filepath = argv[2]

    sctags_ref = SCTagsRef(original_filepath)
    sctags_ref.scan()
    sctags_ref.determine_style()
    sctags_ref.refactor('opposite')
    sctags_ref.commit_changes()
    sctags_ref.save_to_file(new_filepath)

if __name__ == '__main__':
    main()
