from re import findall
from random import choice
from sys import argv
from refactor import Refactorator

class QuotesRef(Refactorator):
    """Handles quotations styles in HTML code"""
    def __init__(self, filepath):
        super().__init__(filepath)

        self.STYLES = ['single', 'double']
        self.OPPOSITES = {
            'single': 'double',
            'double': 'single'
        }
        self.CONVERSIONS = {
            'single-double': self._cnv_single_to_double,
            'double-single': self._cnv_double_to_single
        }


    @staticmethod
    def _cnv_single_to_double(element):
        return '\"' + element[1:-1].replace('\'', '\"') + '\"'


    @staticmethod
    def _cnv_double_to_single(element):
        return '\'' + element[1:-1].replace('\"', '\'') + '\''


    def scan(self):
        """Searches the `code` for the existence of text enclosed in quotes

        Saves all ocurrences in one-dimensional list (`original`).
        """
        found_elements = findall('(\"[^\"]*\")|(\'[^\']*\')', self._code)
        self._original = [  # flatten the `found_elements` list
            group_item \
            for group in found_elements \
            for group_item in group \
            if group_item != ''  # accept if it's not empty result
        ]


    def determine_style(self):
        """Determines `style` based on `original` tags

        For example:
        - '...' '...' is 'single' style
        - "..." "..." is 'double' style
        - "..." '...' is 'incohesive' style
        """
        single_style_occ = 0
        double_style_occ = 0

        for element in self._original:
            if element[0] == '\'':
                single_style_occ += 1
            elif element[0] == '\"':
                double_style_occ += 1

        if single_style_occ != 0 and double_style_occ == 0:
            self._style = 'single'
        elif double_style_occ != 0 and single_style_occ == 0:
            self._style = 'double'
        else:
            self._style = 'incohesive'


### END OF CLASS DECLARATION

def main():
    if len(argv) != 3:
        print('Usage python3 %s <original filepath> <new filepath>')
        return

    original_filepath = argv[1]
    new_filepath = argv[2]

    quotes_ref = QuotesRef(original_filepath)
    quotes_ref.scan()
    quotes_ref.determine_style()
    quotes_ref.refactor('opposite')
    quotes_ref.commit_changes()
    quotes_ref.save_to_file(new_filepath)

if __name__ == '__main__':
    main()
