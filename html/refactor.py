from random import choice

class Refactorator:
    """Base class for all modules responsible for refactoring"""
    
    def __init__(self, filepath):
        """Loads code from the `filepath` file and initialize class members with empty values"""
        with open(filepath, 'r') as f:
            self._code = f.read()  # entire content of the given source file

        self._original = []  # original parts of the code
        self._altered = []  # altered parts of the code
        self._style = ''  # style of the code

        self.STYLES = []  # Names of the coding styles
        self.OPPOSITES = {}  # Pairs {style: opposite_style}
        self.CONVERSIONS = {}  # Pairs {'style1-style2': conversion_function}


    def scan(self):
        """Fill `_original` with elements extracted from `_code`

        Needs to be implemented in derived class.
        """
        raise NotImplementedError('scan method not implemented')


    def determine_style(self):
        """Determine coding style based on elements from `_original`

        Needs to be implemented in derived class.
        """
        raise NotImplementedError('determine_style method not implemented')


    def refactor(self, pick_style):
        """Fills `_altered` with refactored `_original` tags

        `pick_style` parameter is used to set `new_style` and must be one of
        the following:
        * 'opposite' when you want `new_style` to be taken from `OPPOSITES`
        * 'random' when you want `new_style` to be randomly chosen
        * 'unchanged' when you want `new_style` to be the same as the `_style`
        * style name from `STYLES` list
        Otherwise an exception will be raised.

        If `pick_style` is 'opposite' and the `OPPOSITES` hasn't got any entry
        with `_style` as a key, `new_style` is set to 'incohesive'.

        If the `new_style` will be 'incohesive' it will be dynamically chosen
        every time a new element is beeing refactored.

        An exception is raised when the `_original` list is not empty but
        `_style` has not been set yet.
        When `_original` is empty no action is performed.

        As the `CONVERSIONS` dictionary is used to perform conversions,
        it must be properly set in the `__init__` method. Every combination
        of style names from `STYLES` must be persent in the dict. Otherwise
        an exception will be raised.

        This method also raises an exception if it discovers a non-string
        element in `_original` or when a conversion function returns non-string.
        """
        # Checking conditions
        if len(self._original) == 0:
            return
        if len(self._original) > 0 and self._style == '':
            raise ValueError('_original is not empty but _style is not determined')

        for num, style1 in enumerate(self.STYLES):
            for style2 in self.STYLES[:num] + self.STYLES[num+1:]:  # `STYLES` w/out style1
                if (style1 + '-' + style2) not in self.CONVERSIONS.keys():
                    raise KeyError('%s pair is not present in the CONVERSIONS dict' \
                                   % (style1 + '-' + style2))

        # Ensuring every element in `_original` is a string
        for element in self._original:
            if type(element) != str:
                raise ValueError('Non-string element in _original: {}'.format(element))

        # Defining `new_style`
        new_style = None
        if pick_style == 'opposite':
            if self._style != 'incohesive' \
               and self._style in self.OPPOSITES.keys():
                new_style = self.OPPOSITES[self._style]
            else:
                new_style = 'incohesive'
        elif pick_style == 'random':
            new_style = choice(self.STYLES + ['incohesive'])
        elif pick_style == 'unchanged':  # copy `_original` to `_altered` and return
            self._altered = self._original.copy()
            return
        elif pick_style in self.STYLES:
            new_style = pick_style
        else:
            raise ValueError('pick_style parameter: %s is not valid!' % pick_style)

        # Refactoring
        for element in self._original:
            if new_style == 'incohesive':
                dest_style = choice(self.STYLES)
            else:
                dest_style = new_style
                
            # Use conversion function from `CONVERSIONS` mapped to pair '_style-dest_style'
            altered = self.CONVERSIONS[self._style + '-' + dest_style](element)

            # If a conversion function returns a non-string result
            if type(altered) != str:
                raise ValueError('Non-string returned by converion function ' + \
                                 '\'{}\': {}'.format((self._style + '-' + dest_style), altered))
            
            self._altered.append(altered)


    def commit_changes(self):
        """Replaces the `_original` elements in the `_code` with the `_altered` ones
        
        `_original` and `_altered` must have the same length. Otherwise an exception
        will be raised.
        """
        if len(self._original) != len(self._altered):
            raise ValueError('_original and _length have different length')
        
        for old, new in zip(self._original, self._altered):
            self._code = self._code.replace(old, new, 1)  # replace *only* the first occurence


    def save_to_file(self, new_filepath):
        """Saves current state of the `_code` to the file"""
        with open(new_filepath, 'w') as f:
            f.write(self._code)
